package bootwildfly;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWildFlyController {


    @RequestMapping("hello")
    public String sayHello(){
        return ("Hello, SpringBoot on Wildfly");
    }

    @RequestMapping("generror")
    public String sayTrace(){
        try {
            throw new ArithmeticException("dividing a number by 5 is not allowed in this program");
        } catch (ArithmeticException ex) {
            System.out.println(ex);
            ex.printStackTrace();
        }
        return ("Printed Stacktrace on system out and system.err");

    }
}